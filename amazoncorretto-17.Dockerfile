FROM amazoncorretto:17-alpine-jdk

COPY assets/*.sh /opt/gosu/
RUN set -x \
    && apk add --no-cache --virtual .gosu-deps \
		dpkg \
		gnupg \
		openssl \
	&& /opt/gosu/gosu.download.sh \
    && apk del .gosu-deps \
    && rm /tmp/* -r \
    && /opt/gosu/gosu.install.sh \
    && rm -fr /opt/gosu \
    # verify that the binary works
    gosu nobody true

RUN set -x \
  && apk add --no-cache bash \
  && addgroup -g 1100 -S java-app \
  && adduser -S -G 1100 -u 1100 -G java-app java-app \
  && sh -c 'mkdir -p /srv' \
  && sh -c 'chown -R java-app:java-app /srv'
